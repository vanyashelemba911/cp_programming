package Monitors;
import javax.swing.JTextArea;

public class ThreadStateMonitor 
{
    private static Thread MonitorThread;
    
    private static int TickMS = 100;
    private static boolean Working = false;

    private static JTextArea Out;
    private static Thread[] Threads;
    
    private static void ShowInfo()
    {
        while(Threads != null)
        {
            StringBuilder St = new StringBuilder();
            for(int i = 0; Threads != null && i < Threads.length; i++)
            {
                if(Threads[i] != null)
                {
                    St.append(Threads[i].getName()); St.append("\n{\n");
                    St.append("State: "); St.append(Threads[i].getState()); St.append(";\n");
                    St.append("Priority: "); St.append(Threads[i].getPriority()); St.append(";\n");
                    St.append("Alive: "); St.append(Threads[i].isAlive()); St.append(";\n}\n\n");
                }
            }
            Out.setText(St.toString());
            Out.update(Out.getGraphics());
            try
            {
                Thread.sleep(TickMS);
            }
            catch(Exception ex)
            {
                System.out.println(ex.getMessage());
            }
        }
    }
    
    public static void SetTick(int t)throws Exception
    {
        if(t < 0)throw new Exception("(t < 0)");
        TickMS = t;
    }
    
    public static void Start(Thread[] Th, JTextArea Output)throws Exception
    {
        if(Working)throw new Exception("Already Working"); 
        
        Out = Output;
        Threads =  Th;
        
        MonitorThread = new Thread((Runnable)()->{ShowInfo();});
        MonitorThread.start();
        Working = true;
    }
    
    public static void Stop()
    {
        if(Working)
        {   
            StringBuilder St = new StringBuilder();
            for(int i = 0; Threads != null && i < Threads.length; i++)
            {
                if(Threads[i] != null)
                {
                    St.append(Threads[i].getName()); St.append("\n{\n");
                    St.append("State: "); St.append(Threads[i].getState()); St.append(";\n");
                    St.append("Priority: "); St.append(Threads[i].getPriority()); St.append(";\n");
                    St.append("Alive: "); St.append(Threads[i].isAlive()); St.append(";\n}\n\n");
                }
            }
            Out.setText(St.toString());
            Out.update(Out.getGraphics());
            MonitorThread.stop();
            Working = false;
        }
    }
}
