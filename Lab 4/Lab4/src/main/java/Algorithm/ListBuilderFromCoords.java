package Algorithm;

import Entities.AdjacencyList;
import Entities.AdjacencyListNode;
import Entities.Point;
import java.util.ArrayList;

public class ListBuilderFromCoords 
{
    static ArrayList<Point> Points = new ArrayList<Point>();
    static AdjacencyList CurrentList = new AdjacencyList();
    static int SelectedIndex = -1;
    
    private static int GetIndex(Point SomePoint)
    {
        for(int i=0; i < Points.size(); i++)
        {
            if( 
                Points.get(i).X-10 < SomePoint.X && Points.get(i).X+10 > SomePoint.X &&
                Points.get(i).Y-10 < SomePoint.Y && Points.get(i).Y+10 > SomePoint.Y    
              )
                return i;
        }
        return -1;
    }
    public static void Clear()
    {
        SelectedIndex = -1;
        CurrentList = new AdjacencyList();
        Points = new ArrayList<Point>();
    }
    public static int AddCoord(int X , int Y)throws Exception
    {
        Point SomePoint = new Point();
        SomePoint.X = X;
        SomePoint.Y = Y;
        
        if( GetIndex(SomePoint) >= 0 )
        {
            if(SelectedIndex == -1)
            {
                SelectedIndex = GetIndex(SomePoint);
                return -1;
            }
            else
            {
                int index = GetIndex(SomePoint);
                if(SelectedIndex == index) return -1;
                int weight = 
                        (int)Math.sqrt( 
                                Math.pow((Points.get(index).X-Points.get(SelectedIndex).X), 2) + 
                                Math.pow((Points.get(index).Y-Points.get(SelectedIndex).Y), 2) )/10;
                CurrentList.GetNode(index).AddConnection(SelectedIndex, weight);
                CurrentList.GetNode(SelectedIndex).AddConnection(index, weight);
                SelectedIndex = -1;
                return weight;
            }
        }
        else
        {
            CurrentList.AddNode(new AdjacencyListNode( Points.size() ) );
            Points.add(SomePoint);
            return 0;
        }
    }
    public static AdjacencyList GetList()
    {
        return CurrentList;
    }
    public static boolean IsEmpty()
    {
        return CurrentList.IsEmpty();
    }
}
