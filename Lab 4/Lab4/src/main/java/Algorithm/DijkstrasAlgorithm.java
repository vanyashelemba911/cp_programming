package Algorithm;
import Entities.DataAboutNode;
import Entities.Graph;
import Entities.GraphNode;
import java.util.ArrayList;

public class DijkstrasAlgorithm
{    
    ///
    ///
    /// Поля
    private ArrayList<DataAboutNode> NodeData;
    private Thread[] Threads;
    private int ThreadNum;
    private Object SyncObj;
    
    ///
    ///
    /// Конструктори
    public DijkstrasAlgorithm(Graph SomeGraph) throws Exception
    {
        //if`s
        if(SomeGraph == null) throw new Exception("ohh Shit! SomeGraph == null");
        if(SomeGraph.IsEmpty()) throw new Exception("Oh my God, Watson SomeGraph IsEmpty");
        
        //init
        ThreadNum = 1;
        NodeData = new ArrayList<DataAboutNode>();
        SyncObj = new Object();
        InitDataAboutNode(SomeGraph);   
    }

    ///
    ///
    /// Паблік методи керування
    public void SetThreadNum(int num) throws Exception
    {
        if(num > 16 || num < 1) throw new Exception("num is incorrect!");
        ThreadNum = num;
    }
    
    ///
    ///
    /// Паблік методи роботи
    public void FindAWay(int Name_From)throws Exception
    {
        if(!ContainsNodeName(Name_From)) throw new Exception("Skipper, we have a problem");
        UpdateLength(Name_From,0);
        Threads = new Thread[ThreadNum];
        for(int i = 0; i < ThreadNum; i++)
        {
            Threads[i] = new Thread((Runnable)()->{Find();});
            Threads[i].start();
        }  
    }
    
    public String GetResult()throws Exception
    {
        for(int i = 0; i < ThreadNum; i++)
            Threads[i].join();
        
        StringBuilder res = new StringBuilder();
        for(int i = 0; i < NodeData.size(); i++ )
        {
            res.append("\n(");
            res.append(NodeData.get(i).Node.GetName());
            res.append(") -> ");
            res.append(NodeData.get(i).Length);
        }
        return res.toString();
    }
    
    public Thread[] GetThreads()
    {
        return this.Threads;
    }
    
    ///
    ///
    /// Приватні методи алгоритму
    private void Find()
    {
        GraphNode SomeNode = Hey_Buddy_GiveMeNode();
        while(SomeNode != null)
        {
            ArrayList<Integer> NeighborNames = SomeNode.GetNeighbors();
            for( int NeighborName : NeighborNames )
            {
                if(!IsAvalible(NeighborName)) continue;
                if( GetLengthToNodeByName(NeighborName) == -1 )
                {
                    int LengthToNeighbor = SomeNode.GetLengthToNeighbor(NeighborName);
                    int CurrentLength = GetLengthToNodeByName(SomeNode.GetName());
                    UpdateLength(NeighborName, LengthToNeighbor+CurrentLength );
                }
                else
                {
                    int CurrentLength = GetLengthToNodeByName(SomeNode.GetName());
                    int PossibleLength = CurrentLength + SomeNode.GetLengthToNeighbor(NeighborName);
                    if( PossibleLength < GetLengthToNodeByName(NeighborName))
                        UpdateLength(NeighborName,PossibleLength);
                }
                try
                {
                    
                    Thread.sleep(500);
                }
                catch(Exception ex)
                {
                    System.out.println(ex);
                }
            }
          SomeNode = Hey_Buddy_GiveMeNode();
        }
    }
    
    private GraphNode Hey_Buddy_GiveMeNode()
    {
        synchronized(SyncObj)
            {
            ArrayList<Integer> Indexes = new ArrayList<Integer>();
            for(int i = 0; i < NodeData.size(); i++)
            {
                if(NodeData.get(i).Length != -1 && NodeData.get(i).CanSelect)
                {
                    Indexes.add(i);
                }
            }
            if(Indexes.size()>0)
            {
                int MinIndex = Indexes.get(0);
                for (Integer i: Indexes)
                {
                    if(NodeData.get(i).Length < NodeData.get(MinIndex).Length)
                    {
                        MinIndex = i;
                    }
                }
                NodeData.get(MinIndex).CanSelect = false;
                return NodeData.get(MinIndex).Node;
            }
            return null;
        }
    }
    
    ///
    ///
    /// Допоміжні методи
    private boolean ContainsNodeName(int Name)
    {
        for(int i = 0; i < NodeData.size(); i++)
            if( NodeData.get(i).Node.GetName() == Name) 
                return true;
        return false;
    }
    
    private int GetLengthToNodeByName(int Name)
    {
        for(int i = 0; i < NodeData.size(); i++)
            if( NodeData.get(i).Node.GetName() == Name) 
                return NodeData.get(i).Length;
        return Integer.MAX_VALUE;
    }
    
    private boolean IsAvalible(int Name)
    {
        for(int i = 0; i < NodeData.size(); i++)
            if( NodeData.get(i).Node.GetName() == Name) 
                return NodeData.get(i).CanSelect;
        return false;
    }
    
    private void UpdateLength(int Name,int Value)
    {
        for(int i = 0; i < NodeData.size(); i++)
        {
            if( NodeData.get(i).Node.GetName() == Name) 
            {
                NodeData.get(i).Length = Value;
                return;
            }
        }
    }
    
    private void InitDataAboutNode(Graph SomeGraph)throws Exception
    {
        for(int i = 0; i < SomeGraph.GetAllNodes().size();i++)
            NodeData.add(new DataAboutNode(SomeGraph.GetNode(i),-1,true));
    }
}
