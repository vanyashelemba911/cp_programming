package Entities;

public class Point 
{
    public int X;
    public int Y;

    @Override 
    public boolean equals(Object obj) 
    {
        return (this.X == ((Point)obj).X && this.Y == ((Point)obj).Y );
    }
}
