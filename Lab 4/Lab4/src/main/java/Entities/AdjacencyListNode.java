package Entities;
import java.util.HashMap;

public class AdjacencyListNode 
{
    private int NodeID;
    private HashMap<Integer,Integer> Connections;
    
    public AdjacencyListNode(int NodeID)
    {
        this.NodeID = NodeID;
        this.Connections = new HashMap<Integer,Integer>();
    }
    
    public void AddConnection(Integer ID , Integer Weight)throws Exception
    {
        if(Weight < 0)
            throw new Exception("Yeah Buddy! Light weight baby!!");
        if(ID < 0)
            throw new Exception("wtf?! ID < 0");
        
        Connections.put(ID, Weight);
    }
    
    public int Get_NodeID()
    {
        return this.NodeID;
    }
    
    public HashMap<Integer,Integer> Get_Connections() throws Exception
    {
        if(Connections == null)
            throw new Exception("Oh shit, we're isolated");
        
        return this.Connections;
    }
    
    @Override 
    public boolean equals(Object obj) 
    {
        return (this.NodeID == ((AdjacencyListNode)obj).NodeID);
    }
}
