package Entities;
import static java.lang.Character.isDigit;
import java.util.ArrayList;

public class AdjacencyList // Список суміжностей
{
    private ArrayList<AdjacencyListNode> Nodes;
    
    public AdjacencyList()
    {
       this.Nodes = new ArrayList<AdjacencyListNode>();
    }
    
    public void InitializeFromString(String AdjacencyListString)throws Exception
    {
        if(AdjacencyListString.isEmpty()) 
            throw new Exception("Empty string in InitializeFromString()");
        
        Nodes = ParseFromString(AdjacencyListString);
    }
    
    public boolean Contains(AdjacencyListNode Node)
    {
        if(Nodes == null) return false;
        return Nodes.contains(Node);
    }
    
    public boolean IsEmpty()
    {
        if(Nodes == null) return false;
        return Nodes.size()<=0;
    }
    
    public void AddNode(AdjacencyListNode Node)throws Exception
    {
        if(Nodes == null) throw new Exception("Nodes == null!!!");
        if(Node == null) throw new Exception("Node == null!!!");
        if(Nodes.contains(Node)) throw new Exception("Node already exist");
        
        Nodes.add(Node);
    }
    
    public void RemoveNode(AdjacencyListNode Node)throws Exception
    {
        if(Nodes == null) throw new Exception("Nodes == null!!!");
        if(Node == null) throw new Exception("Node == null!!!");
        if(!Nodes.contains(Node)) throw new Exception("The AdjacencyList has no such node");
        
        Nodes.remove(Node);
    }
    
    public int Capacity()
    {
        if(Nodes == null) return -1;
        return Nodes.size();
    }
    
    public AdjacencyListNode GetNode(int id)throws Exception
    {
        if(Nodes == null) throw new Exception("Nodes == null!!!");
        if(id < 0 || id > Capacity()) throw new Exception("Wrong id in GetNode()");

        return Nodes.get(id);
    }
    
    private ArrayList<AdjacencyListNode> ParseFromString(String AdjacencyListString)throws Exception
    {
        ArrayList<AdjacencyListNode> NewNodes = new ArrayList<AdjacencyListNode>();
        AdjacencyListNode TempNode;
        
        AdjacencyListString = AdjacencyListString.replaceAll("\\s+","");

        int i = 0;
        while(i<AdjacencyListString.length())
        {
            if(isDigit(AdjacencyListString.charAt(i)))
            {
                String tmp = "";
                while(isDigit(AdjacencyListString.charAt(i)))
                {
                    tmp+=AdjacencyListString.charAt(i);
                    i++;
                }
                TempNode = new AdjacencyListNode(Integer.parseInt(tmp));
                if(AdjacencyListString.charAt(i) == '-' && AdjacencyListString.charAt(i+1) == '>')
                {
                    i+=2;
                    while(i<AdjacencyListString.length())
                    {
                        tmp = "";
                        while(isDigit(AdjacencyListString.charAt(i)))
                        {
                            tmp+=AdjacencyListString.charAt(i);
                            i++;
                        }
                        int ConnectedNodeID = Integer.parseInt(tmp);
                        if(AdjacencyListString.charAt(i) != '(') throw new Exception("String syntax error");
                        i++;
                        tmp = "";
                        while(isDigit(AdjacencyListString.charAt(i)))
                        {
                            tmp+=AdjacencyListString.charAt(i);
                            i++;
                        }
                        TempNode.AddConnection(ConnectedNodeID, Integer.parseInt(tmp));
                        if(AdjacencyListString.charAt(i) != ')')throw new Exception("String syntax error");
                        i++;
                        if(AdjacencyListString.charAt(i) == ';')
                        {
                            i++;
                            break;
                        }
                        i++;
                    }
                    NewNodes.add(TempNode);
                }
                else
                {
                    throw new Exception("String syntax error");
                }
            }
            else
            {
                throw new Exception("String syntax error");
            }
        }
        
        return NewNodes;
    }

}
