package Entities;

import java.util.ArrayList;
import java.util.HashMap;

public class Graph 
{
    private ArrayList<GraphNode> Nodes;
    
    public Graph()
    {
        Nodes = new ArrayList<GraphNode>();
    }
    
    public boolean IsEmpty()
    {
        if(Nodes == null) return true;
        return Nodes.isEmpty();
    }
    private void AddNode(GraphNode n)throws Exception
    {
        if(Nodes == null ) 
            throw new Exception("Nodes == null in GetAllNodes()");
        if(!Nodes.contains(n)) Nodes.add(n);
    }
    public ArrayList<GraphNode> GetAllNodes( )throws Exception
    {
        if(Nodes == null ) 
            throw new Exception("Nodes == null in GetAllNodes()");
        return Nodes;
    }
    public GraphNode GetNode(int id)throws Exception
    {
        if(Nodes == null ) 
            throw new Exception("Nodes == null in GetNode()");
        if(id < 0 || id > Nodes.size()) 
            throw new Exception("Wrong id in GetNode(int id);");
        
        return Nodes.get(id);
    }
    public void InitializeFromAdjacencyList(AdjacencyList AdjList) throws Exception
    {

        if(AdjList == null || AdjList.IsEmpty()) 
            throw new Exception("InitializeFromAdjacencyList Failed!");
        if(Nodes == null) 
            Nodes = new ArrayList<GraphNode>();
        if(Nodes.size()>0)
            Nodes.clear();

        for(int i = 0; i < AdjList.Capacity(); i++ )
        {
            AdjacencyListNode AdjListNode = AdjList.GetNode(i);
            GraphNode TmpNode = new GraphNode(AdjListNode.Get_NodeID());
            HashMap<Integer,Integer> Connections = AdjListNode.Get_Connections();
            for(int ConnectedNodeID : Connections.keySet())
            {
                TmpNode.AddNeighbor(ConnectedNodeID, Connections.get(ConnectedNodeID));
            }
            Nodes.add(TmpNode);
        }
    }
}
