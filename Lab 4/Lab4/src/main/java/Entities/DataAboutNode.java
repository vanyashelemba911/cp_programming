package Entities;
public class DataAboutNode 
{
    public GraphNode Node;
    public Integer Length;
    public Boolean CanSelect;
    
    public DataAboutNode()
    {
        Node = null;
        Length=-1;
        CanSelect = true;        
    }
    
    public DataAboutNode(GraphNode Node, Integer Length, Boolean CanSelect)
    {
        this.Node = Node;
        this.Length=Length;
        this.CanSelect = CanSelect;        
    }
    
}
