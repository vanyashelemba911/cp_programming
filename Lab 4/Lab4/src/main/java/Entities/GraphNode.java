package Entities;

import java.util.ArrayList;
import java.util.HashMap;

public class GraphNode 
{
    private int Name;
    private HashMap<Integer,Integer> Neighbors;
    
    GraphNode(int Name)
    {
        this.Name = Name;
        this.Neighbors = new HashMap<Integer,Integer>();
    }
    
    public void AddNeighbor(int NeighborName,int Length)
    {
        this.Neighbors.put(NeighborName,Length);
    }
    
    public ArrayList<Integer> GetNeighbors()
    {
        ArrayList<Integer> ret = new ArrayList<Integer>();
        
        for(Integer NeighborName : Neighbors.keySet())
        {
            ret.add(NeighborName);
        }
        
        return ret;
    }
    
    public int GetName()
    {
        return Name;
    }
    
    public int GetLengthToNeighbor(int Name)
    {
        return this.Neighbors.get(Name);
    }
}
