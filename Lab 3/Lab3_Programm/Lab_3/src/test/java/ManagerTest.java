/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Ivan
 */
public class ManagerTest {
    
    public ManagerTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    
    @Test
    public void testGet_MostPopularWord()
    {
        Manager instance = new Manager();
        String Text = "aaa bbb aaa bbb aaa";
        String expResult = "aaa";
        assertEquals(instance.Get_MostPopularWord(Text), expResult);
    }
    
    @Test
    public void testTask2()
    {
        Manager instance = new Manager();
        String Text = " Хіба можна не любити свою Батьківщину? Що місяцю зіроньки кажуть ясненькі?";
        int len = 6;
        String expRes = " місяцю 	 любити 	 кажуть 	";
        String res = instance.Task_2(Text,len);
        System.out.println(res);
        assertEquals( expRes , res);
    }
}
