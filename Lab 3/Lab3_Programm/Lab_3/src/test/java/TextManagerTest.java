/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Ivan
 */
public class TextManagerTest {
    
    public TextManagerTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of LoadFromFile method, of class TextManager.
     */
    @Test
    public void testLoadFromFile() {
        System.out.println("LoadFromFile");
        String FileName = "FileForTests.txt";
        String expResult = "TestInfo ";
        String result = TextManager.LoadFromFile(FileName);
        assertEquals(expResult, result);
    }
    
}
