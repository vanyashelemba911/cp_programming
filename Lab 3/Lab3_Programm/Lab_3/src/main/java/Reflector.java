import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
public class Reflector 
{
    public static boolean Show_Message(String Msg, javax.swing.JTextArea Output)
    {
        Output.append(Msg);
        Output.append("\n");
        return true;
    }
    public static boolean Show_StringList(ArrayList<String> Arr ,javax.swing.JTextArea Output)
    {
        for(String st : Arr) Show_Message(st,Output);
            
        return true;
    }
    public static boolean Clear_Area(javax.swing.JTextArea Output)
    {
        Output.selectAll();
        Output.replaceSelection("");
        return true;
    }
    public static boolean Clear_Table(javax.swing.JTable Output)
    {
        javax.swing.table.DefaultTableModel model = (javax.swing.table.DefaultTableModel) Output.getModel();
        model.setRowCount(0);
        return true;
    }
}
