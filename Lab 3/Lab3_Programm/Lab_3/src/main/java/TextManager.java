import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
public class TextManager {
    static String LoadFromFile(String FileName)
    {
        String ret = "";
        try{
            File SomeFile = new File(FileName);
            Scanner FileScann = new Scanner(SomeFile);
            while (FileScann.hasNextLine()) 
            {
                ret += FileScann.nextLine()+" ";
            }
        }
        catch(FileNotFoundException e)
        {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return ret;
    }
}
