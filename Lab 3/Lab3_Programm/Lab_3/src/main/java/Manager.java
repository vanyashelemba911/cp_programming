import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Manager {
    String File_1;
    String Text_From_File;
    String Separators = " ?.,;:-+\\n\\t{}[]=()<>*&%$#@\\\"\\'`~!";
    private class Hierarchy_Info
    {
        public Hierarchy_Info(String s,int w){ Sentence = s; Weight = w; }
        public Hierarchy_Info(){}
        public String Sentence;
        public int Weight;
    }
    ArrayList<Hierarchy_Info> Hierarchy;
    //==========================================================================
    public Manager(){Construct("","");}
    public Manager(String File1){Construct(File1,"");}
    //==========================================================================
    private void Construct(String File1 , String Text)
    {
        this.File_1 = File1;
        this.Text_From_File = Text;
        Hierarchy = new ArrayList<Hierarchy_Info>();
    }
    //==========================================================================
    public boolean Initialize(String File1)
    {
        this.File_1 = File1;
        return Initialize();
    }
    public boolean Initialize()
    {
        if(File_1 == null || File_1.isEmpty()) return false;
        Text_From_File = TextManager.LoadFromFile(File_1);
        return true;
    }
    private void AddToHierarchy(String st , int weight)
    {
        Hierarchy.add(new Hierarchy_Info("«" + st + "»" , weight));
        String Pattern = "";
        for(int i = 0; i < st.length();i++ )
        {
            if(st.charAt(i) == '«')
            {
                int k = 1;
                Pattern = "";
                i++;
                while(i < st.length() && ( k > 0) )
                {
                    Pattern += st.charAt(i);
                    i++;
                    if(st.charAt(i) == '«') k++;
                    if(st.charAt(i) == '»') k--;
                }
                String tmp;
                tmp = Pattern;
                AddToHierarchy(Pattern,weight+1);
            }
        }
    }
    public ArrayList<String> Get_Hierarchy()
    {
        ArrayList<String> ret = new ArrayList<String>();
        for(Hierarchy_Info Hi : Hierarchy)
        {
            ret.add(Hi.Sentence + " - " + Hi.Weight);
        }
        return ret;
    }
    private String MostPopularWord()
    {
        String ret = "";
        StringTokenizer st = new StringTokenizer(Text_From_File,Separators+"«»");
        HashMap<String,Integer> AllWords = new HashMap<String,Integer>();
        while (st.hasMoreTokens()) 
        {
            String tmp = st.nextToken();
            if(AllWords.containsKey(tmp))
            {
                AllWords.put(tmp, AllWords.get(tmp)+1);
            }
            else
            {
                AllWords.put(tmp, 1);
            }            
        }
        int max = 0;
        for (String SomeKey : AllWords.keySet())
        {
            if(SomeKey.length() > 2 && AllWords.get(SomeKey)>max)
            {
                max = AllWords.get(SomeKey);
                ret = SomeKey;
            }
        }
        return ret;
    }
    
    public String Get_MostPopularWord(String st)
    {
        Text_From_File = st;
        return MostPopularWord();
    }
    
    public String Get_MostPopularWord()
    {
        return MostPopularWord();
    }
    public boolean Task_1(String w)
    {
        String[] words = w.split(" ");
        String Pattern = "";
        Text_From_File = Text_From_File.replaceAll("  ", " ");
        for(int i = 0; i < Text_From_File.length();i++ )
        {
            if(Text_From_File.charAt(i) == '«')
            {
                int k = 1;
                Pattern = "";
                i++;
                while(i < Text_From_File.length() && ( k > 0) )
                {
                    Pattern += Text_From_File.charAt(i);
                    i++;
                    if(Text_From_File.charAt(i) == '«') k++;
                    if(Text_From_File.charAt(i) == '»') k--;
                }
                String tmp;
                tmp = Pattern;
                AddToHierarchy(Pattern,1);
                for(String SomeString : words)
                    Pattern = Pattern.replaceAll(SomeString, MostPopularWord());
                Text_From_File = Text_From_File.replaceAll(tmp,Pattern);
            }
        }
        return true;
    }
    public String Task_2(String Text, int len)
    {
        String ret = "";
        HashSet<String> words = new HashSet<String>();
        Pattern QSentence = Pattern.compile("\\s.*[?]");
        
        Matcher QSentence_matcher = QSentence.matcher(Text);
        
        while (QSentence_matcher.find()) {
            Pattern Good_Words = Pattern.compile("\\s{1}[a-яА-Я[і,І]]{"+len+"}\\s{1}");
            Matcher Good_Words_matcher = Good_Words.matcher(Text.substring(QSentence_matcher.start(), QSentence_matcher.end()));
            while (Good_Words_matcher.find())
                words.add( Text.substring(Good_Words_matcher.start(), Good_Words_matcher.end()) );
        }
        for(String str : words) ret += str+"\t";
        return ret;
    }
    public String Get_Text(){return Text_From_File;}
    //==========================================================================
}
