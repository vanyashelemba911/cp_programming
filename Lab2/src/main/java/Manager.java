import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Manager 
{
    private ArrayList<Teacher> Teachers;
    private ArrayList<String> Students_On_AllC;
    private HashMap<String,ArrayList<String>> Students;
    private ArrayList<String> CommonStudents;
    private ArrayList<String> MostCourses;
    //==========================================================================
    
    public Manager()
    {
        Teachers = new ArrayList<Teacher>();
        Students = new HashMap<String,ArrayList<String>>();
        Students_On_AllC = new ArrayList<String>();
        CommonStudents = new ArrayList<String>();
        MostCourses = new ArrayList<String>();
    }
    //==========================================================================
    
    public ArrayList<Teacher> Get_Teachers()
    {
        return (ArrayList<Teacher>)Teachers.clone();
    }
    public ArrayList<String> Get_MostCourses()
    {
        return (ArrayList<String>)MostCourses.clone();
    }
    public HashMap<String,ArrayList<String>> Get_Students()
    {
        return (HashMap<String,ArrayList<String>>)Students.clone();
    }
    public ArrayList<String> Get_Students_On_AllC()
    {
        return (ArrayList<String>)Students_On_AllC.clone();
    }
    public ArrayList<String> Get_CommonStudents()
    {
        return (ArrayList<String>)CommonStudents.clone();
    }
    //==========================================================================
    
    private boolean Add_Students(ArrayList<Course> Courses)
    {
        for(Course SomeCourse : Courses)
        {
            ArrayList<String> StudentNames = SomeCourse.GetStudents();
            for(String SomeStudent : StudentNames)
            {
                if(Students.containsKey(SomeStudent))
                {
                    Students.get(SomeStudent).add(SomeCourse.GetName());
                }
                else
                {
                    Students.put(SomeStudent, new ArrayList());
                    Students.get(SomeStudent).add(SomeCourse.GetName());
                }
            }
        }
        return true;
    }
    private boolean Initialize_Students()
    {
        if(Teachers == null || Teachers.isEmpty()) return false;
        for(Teacher SomeTeacher : Teachers)
        {
            Add_Students(SomeTeacher.GetCourses());
        }
        return true;
    }
    private boolean ClearAll()
    {
        Teachers.clear();
        Students_On_AllC.clear();
        Students.clear();
        return true;
    }
    private boolean Initialize_Students_On_AllC()
    {
        HashSet <String> Courses = new HashSet<String>();
        for(Teacher SomeTeacher : Teachers)
        {
            for(Course SomeCourse : SomeTeacher.GetCourses())
            {
                Courses.add( SomeCourse.GetName() );
            }
        }
        Set<String> Keys = Students.keySet();
        for(String SomeKey : Keys)
        {
            if(Students.get(SomeKey).size() == Courses.size())
            {
                Students_On_AllC.add(SomeKey);
            }
        }
        return true;
    }
    private boolean Init_MostCourses()
    {
        int middle = 1;
        int Summ = 0;
        int k = 0;
        for(Teacher SomeTeacher : Teachers)
        {
            Summ+=SomeTeacher.GetCourses().size();
            k++;
        }
        middle = Summ/k;
        for(Teacher SomeTeacher : Teachers)
        {
            if(SomeTeacher.GetCourses().size() > middle)
            {
                MostCourses.add( SomeTeacher.GetName() );
            }
        }
        return true;
    }
    private boolean Init_CommonStudents(String D1,String D2)
    {
        Course C1 = null,C2=null;
        boolean b = true;
        for(Teacher SomeTeacher : Teachers)
        {
            for(Course SomeCourse : SomeTeacher.GetCourses())
            {
                if(SomeCourse.GetName().equals(D1) || SomeCourse.GetName().equals(D2))
                {
                    if(b)
                    {
                        C1 = SomeCourse;
                        b = false;
                    }
                    else
                    {
                        C2 = SomeCourse;
                        break;
                    }
                }
            }
            
        }
        if(C1 == null || C2 == null) return false;
        for(String SomeStudent:C1.GetStudents())
        {
            if(C2.GetStudents().contains(SomeStudent))
                CommonStudents.add(SomeStudent);
        }
        return true;
    }
    private boolean Init_FromFile(String File1Name , String File2Name )
    {
        if(File1Name == null || File1Name.isEmpty() || File2Name == null || File2Name.isEmpty())
        {
            return false;
        }
        if ( !FileReader.Read_TeacherData(File1Name, Teachers)) return false;
        if ( !FileReader.Read_TeacherData(File2Name, Teachers)) return false;
        return true;
    }
    public boolean Initialize( String File1Name , String File2Name , String D1 , String D2 )
    {
        ClearAll();        
        Init_FromFile(File1Name,File2Name);
        Initialize_Students();
        Initialize_Students_On_AllC();
        Init_CommonStudents(D1,D2);
        Init_MostCourses();
        return true;
    }
    
    //==========================================================================
}