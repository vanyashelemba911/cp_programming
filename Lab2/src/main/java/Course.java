
import java.util.ArrayList;

public class Course {
    private String Name;
    private ArrayList<String> Students;
    public Course(String Name , ArrayList<String> Students)
    {
        this.Name = Name;
        this.Students = Students;
    }
    public boolean Add_Student(String StudentName)
    {
        if(Students == null) return false;
        Students.add(StudentName);
        return true;
    }
    public String GetName() 
    {
        return Name;
    }
    public ArrayList<String> GetStudents()
    {
        return Students;
    }
}
