import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class FileReader {
    public static boolean Read_TeacherData(String FileName,ArrayList<Teacher> Teachers)
    {
        if(Teachers == null || FileName == null || FileName.isEmpty()) return false;
         
        try
        {
            File DataFile = new File(FileName);
            Scanner FileScann = new Scanner(DataFile);
            String TeacherName;
            String CourseName = "";
            ArrayList<Course> Courses = new ArrayList<Course>();
            ArrayList<String> StudentNames = new ArrayList<String>();
            while (FileScann.hasNextLine()) 
            {
                String FileLine = FileScann.nextLine();
                TeacherName = FileLine;
                System.out.println(TeacherName);
                if(FileScann.hasNextLine())FileScann.nextLine();
                FileLine = FileScann.nextLine();
                Courses.clear();
                while(FileScann.hasNextLine() && !FileLine.equals("}"))
                {
                    CourseName = FileLine;
                    StudentNames.clear();
                    FileScann.nextLine();
                    FileLine = FileScann.nextLine();
                    while(FileScann.hasNextLine() && !FileLine.equals("}"))
                    {
                        StudentNames.add(FileLine);
                        FileLine = FileScann.nextLine();
                    }
                    if(FileScann.hasNextLine()) FileLine = FileScann.nextLine();
                    Courses.add(new Course(CourseName,Copier.GetCopy_String(StudentNames)));
                }
                Teachers.add(new Teacher(TeacherName,Copier.GetCopy_Course(Courses)));
            }
            FileScann.close();
            }
        catch(FileNotFoundException e)
        {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return true;
    }
}
/*


*/