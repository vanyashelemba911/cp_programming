import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
public class Reflector {
        public static boolean Show_Teachers(ArrayList<Teacher> Teachers, javax.swing.JTextArea Output)
    {
        for(Teacher SomeTeacher : Teachers)
        {
            Output.append("\nВикладач: " + SomeTeacher.GetName()+"\n");
            for(Course SomeCourse : SomeTeacher.GetCourses())
            {
                Output.append("\n");
                Output.append("\tВикладає: \"" + SomeCourse.GetName()+"\"\n");
                Output.append("\tСтуденти записані на цей курс: \n\t{\n");
                for(String StudentName : SomeCourse.GetStudents() )
                {
                    Output.append("\t\t"+StudentName+"\n");
                }
                Output.append("\t}\n");
            }
            Output.append("\n");
        }
        return true;
    }
    private static String List_To_String(ArrayList<String> Arr)
    {
        String st = new String();
        st = "";
        for(int i = 0; i < Arr.size()-1; i++)
        {
            st += Arr.get(i) + ", ";
        }
        st += Arr.get(Arr.size()-1);
        return st;
    }
    public static boolean Show_Students(HashMap<String,ArrayList<String>> Students, javax.swing.JTable Output)
    {
        javax.swing.table.DefaultTableModel model = (javax.swing.table.DefaultTableModel) Output.getModel();
        Set<String> Keys = Students.keySet();
        for(String SomeKey : Keys)
        {
            model.addRow(new String[]{SomeKey,List_To_String(Students.get(SomeKey))});
        }               
        return true;
    }
    public static boolean Show_Message(String Msg, javax.swing.JTextArea Output)
    {
        Output.append(Msg);
        Output.append("\n");
        return true;
    }
    public static boolean Show_StringList(ArrayList<String> Arr ,javax.swing.JTextArea Output)
    {
        for(String st : Arr) Show_Message(st,Output);
            
        return true;
    }
    public static boolean Clear_Area(javax.swing.JTextArea Output)
    {
        Output.selectAll();
        Output.replaceSelection("");
        return true;
    }
    public static boolean Clear_Table(javax.swing.JTable Output)
    {
        javax.swing.table.DefaultTableModel model = (javax.swing.table.DefaultTableModel) Output.getModel();
        model.setRowCount(0);
        return true;
    }
}
