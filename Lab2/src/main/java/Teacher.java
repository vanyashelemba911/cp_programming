import java.util.ArrayList;

public class Teacher 
{
    private String Name;
    private ArrayList<Course> Courses;
    //==========================================================================
    
    public Teacher(String Name, ArrayList<Course> Courses)
    {
        this.Name = Name;
        this.Courses = Courses;
    }
    
    //==========================================================================
    
    public String               GetName(){return Name;}
    public ArrayList<Course>    GetCourses(){return (ArrayList<Course>)Courses.clone();}
    //==========================================================================
    
    public void Add_Course(Course SomeCourse)
    {
        if( this.Courses == null ) return;
        this.Courses.add(SomeCourse);
    }
    
    //==========================================================================
    
}
