
import java.util.ArrayList;


public class Copier {
    public static ArrayList<Course> GetCopy_Course(ArrayList<Course> smth)
    {
        ArrayList<Course> NewList = new ArrayList<Course>();
        for( Course SomeCourse : smth )
        {
            NewList.add(new Course(SomeCourse.GetName(),GetCopy_String(SomeCourse.GetStudents())));
        }
        return NewList;
    }
    public static ArrayList<String> GetCopy_String(ArrayList<String> smth)
    {
        ArrayList<String> NewList = new ArrayList<String>();
        for( String SomeString : smth )
        {
            NewList.add(SomeString);
        }
        return NewList;
    }
}
