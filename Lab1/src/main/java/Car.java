public class Car implements Comparable<Car>
{
    private String CType;
    private String Country;
    private String Brand;
    private String Model;
    private String City;
    private String Color;
    private String Year;
    private String Price;
    private String Car_Milieage;
    private String cState;
    private String FType;
    private String FuelConsumption;
    private String Engine_Capacity;
    //==========================================================================
    public String get_TypeOfCar(){return CType;};
    public String get_Country(){return Country;};
    public String get_Brand(){return Brand;};
    public String get_Model(){return Model;};
    public String get_City(){return City;};
    public String get_Color(){return Color;};
    public String get_Year(){return Year;};
    public String get_Price(){return Price;};
    public String get_Car_Milieage(){return Car_Milieage;};
    public String get_FuelConsumption(){return FuelConsumption;};
    public String get_Engine_Capacity(){return Engine_Capacity;};
    public String get_cState(){return cState;};
    public String get_TypeOfFuel(){return FType;};
    //==========================================================================
    public boolean set_TypeOfCar(String a)   {this.CType = a; return true;};
    public boolean set_Country(String a)        {this.Country = a; return true;};
    public boolean set_Brand(String a)          {this.Brand = a; return true;};
    public boolean set_Model(String a)          {this.Model = a; return true;};
    public boolean set_City(String a)           {this.City = a; return true;};
    public boolean set_Color(String a)          {this.Color = a; return true;};
    public boolean set_Year(String a)              {this.Year = a; return true;};
    public boolean set_Price(String a)             {this.Price = a; return true;};
    public boolean set_Car_Milieage(String a)      {this.Car_Milieage = a; return true;};
    public boolean set_FuelConsumption(String a){this.FuelConsumption = a; return true;};
    public boolean set_Engine_Capacity(String a){this.Engine_Capacity = a; return true;};
    public boolean set_cState(String a)       {this.cState = a; return true;};
    public boolean set_TypeOfFuel(String a) {this.FType = a; return true;};
    //==========================================================================
    public boolean Initialize( 
            String Brand,
            String Model,
            String CType,
            String Country,
            String City,
            String Color,
            String Year,
            String Price,
            String Car_Milieage,
            String cState,
            String FType,
            String FuelConsumption,
            String Engine_Capacity)
    {
        this.CType = CType;
        this.Country = Country;
        this.Brand = Brand;
        this.Model = Model;
        this.City = City;
        this.Color = Color;
        this.Year = Year;
        this.Price = Price;
        this.Car_Milieage = Car_Milieage;
        this.cState = cState;
        this.FType = FType;
        this.FuelConsumption = FuelConsumption;
        this.Engine_Capacity = Engine_Capacity;
        return true;
    }
    public Car()
    {
        this.CType = "----";
        this.Country = "----";
        this.Brand = "----";
        this.Model = "----";
        this.City = "----";
        this.Color = "----";
        this.Year = "----";
        this.Price = "----";
        this.Car_Milieage = "----";
        this.cState = "----";
        this.FType = "----";
        this.FuelConsumption = "----";
        this.Engine_Capacity = "----";
    }
    public Car( 
            String Brand,
            String Model,
            String CType,
            String Country,
            String City,
            String Color,
            String Year,
            String Price,
            String Car_Milieage,
            String cState,
            String FType,
            String FuelConsumption,
            String Engine_Capacity)
    {
       this.Initialize(Brand, Model, CType, Country, City, Color, Year, Price, Car_Milieage, cState, FType, FuelConsumption, Engine_Capacity);
    }
    public Car(Car SomeCar)
    {
        this.Initialize(SomeCar.Brand, SomeCar.Model, SomeCar.CType, SomeCar.Country, SomeCar.City, SomeCar.Color, SomeCar.Year, SomeCar.Price, 
                SomeCar.Car_Milieage, SomeCar.cState, SomeCar.FType, SomeCar.FuelConsumption, SomeCar.Engine_Capacity);
    }

    @Override
    public int compareTo(Car t) 
    {
        return this.Brand.compareTo(t.Brand);
    }
}