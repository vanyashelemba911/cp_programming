import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.Comparator;

public class Manager {
    //================================================
    javax.swing.JTable Output;
    ArrayList<Car> Cars;
    ArrayList<Car> CarsForShow;
    //===============================================
    public Manager(javax.swing.JTable Table1)
    {
        this.Output = Table1;
      
    }
    //===============================================
    public class CarMilieageComparator extends CarComparator implements Comparator<Car>
    {
        @Override
        public int compare(Car t, Car t1) {
            if(Grow)
                return Integer.parseInt(t.get_Car_Milieage()) - Integer.parseInt(t1.get_Car_Milieage());
            else
                return Integer.parseInt(t1.get_Car_Milieage()) - Integer.parseInt(t.get_Car_Milieage());
            
        }
    }
    public static class CarYearComparator extends CarComparator  implements Comparator<Car>
    {
        @Override
        public int compare(Car t, Car t1) {
            if(Grow)
                return Integer.parseInt(t.get_Year()) - Integer.parseInt(t1.get_Year());
            else
                return Integer.parseInt(t1.get_Year()) - Integer.parseInt(t.get_Year());
        }
    }
    public static class CarCapacityComparator extends CarComparator  implements Comparator<Car>
    {
        @Override
        public int compare(Car t, Car t1) {
            double res = 0.0;
            if(Grow)
                res = Double.parseDouble(t.get_Engine_Capacity()) - Double.parseDouble(t1.get_Engine_Capacity());
            else
                res = Double.parseDouble(t1.get_Engine_Capacity()) - Double.parseDouble(t.get_Engine_Capacity());             
            return (res>0)?1:(res==0)?0:-1;
        }
    }
    
    //===============================================
    private boolean LoadFromDB()
    {
        Cars = new ArrayList<Car>();
        try 
        {
            String[] CarData = new String[14];
            File FileDB = new File("DataBase.txt");
            Scanner myReader = new Scanner(FileDB);
            int i = 0;
            System.out.println("Reading from file....");
            while (myReader.hasNextLine()) {
                CarData[i] = myReader.nextLine();
                System.out.println(CarData[i]);                              
                if(i == 13)
                {
                  i = 0;
                  System.out.println("[Car]");
                  for(int j = 0; j < 13 ; j++)
                  {
                    System.out.println("  "+CarData[j]);
                  }
                  Cars.add(
                          new Car(CarData[0],CarData[1],CarData[2],
                                  CarData[3],CarData[4],CarData[5],
                                  CarData[6],CarData[7],CarData[8],
                                  CarData[9],CarData[10],CarData[11],
                                  CarData[12]) );
                  System.out.println("->>>>"+Cars.get(Cars.size()-1).get_Brand());
                }
                else
                  i++;
            }
            Cars.add(
                          new Car(CarData[0],CarData[1],CarData[2],
                                  CarData[3],CarData[4],CarData[5],
                                  CarData[6],CarData[7],CarData[8],
                                  CarData[9],CarData[10],CarData[11],
                                  CarData[12]) );
            myReader.close();
        } 
        catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return true;
    }
    public boolean InitializeCars()
    {
        if(this.CarsForShow != null)
        {
          this.CarsForShow.clear();
          this.CarsForShow = null;
        }
        if(this.Cars != null)
        {
          this.Cars.clear();
          this.Cars = null;
        }
        try{
            if( !LoadFromDB() )
            {
                throw new SomeException(1);
            }
            this.CarsForShow = new ArrayList<Car>();
            for(Car SomeCar : Cars)
            {
                CarsForShow.add(SomeCar);
            }
        }
        catch(SomeException exc)
        {
          System.out.println(exc.getMessage());
        }
        
        return true;
    }
    
    public boolean SortCars(ParameterToSort Param,boolean Grow)
    {
        if(Param == null) return false;
        if(this.CarsForShow == null)
        {
            if( this.Cars == null )
                return false;
            else
            {
                this.CarsForShow = new ArrayList<Car>();
                for(Car SomeCar : Cars)
                {
                    CarsForShow.add(SomeCar);
                }
            }
        }
        //================================================
        CarComparator SomeComparator;
        switch(Param)
        {
            case byBrand: // сортування з використанням лямбда виразів
            {
                if(Grow)
                    Collections.sort(CarsForShow,(t, t1) -> t.get_Brand().compareTo(t1.get_Brand()));
                else
                    Collections.sort(CarsForShow,(t, t1) -> t1.get_Brand().compareTo(t.get_Brand()));
            } break;
            case byYear:// static inner class
            {
                SomeComparator = new CarYearComparator();
                SomeComparator.Grow = Grow;
                Collections.sort(CarsForShow,SomeComparator);
            }break;
            case byPrice: // anonymous class
            {
                SomeComparator = new CarComparator()
                {
                    @Override
                    public int compare(Car t, Car t1) {
                        if(Grow)
                            return Integer.parseInt(t.get_Price()) - Integer.parseInt(t1.get_Price());
                        else
                            return Integer.parseInt(t1.get_Price()) - Integer.parseInt(t.get_Price());
                    }
                };
                SomeComparator.Grow = Grow;
                Collections.sort(CarsForShow,SomeComparator);
            }break;
            case byMilieage://inner class
            {
                SomeComparator = new CarMilieageComparator();
                SomeComparator.Grow = Grow;
                Collections.sort(CarsForShow,SomeComparator);
            }break;
            case byCapacity:
            {
                SomeComparator = new CarCapacityComparator();
                SomeComparator.Grow = Grow;
                Collections.sort(CarsForShow,SomeComparator);
            }break;
        }
        return true;
    }
    public boolean FilterCars(CarFilter SomeFilter)
    {
        if(this.CarsForShow == null)
        {
            this.CarsForShow = new ArrayList<Car>();
        }
        else
        {
          this.CarsForShow.clear();
        }
        
        for(Car SomeCar : this.Cars)
        {
            if(
                ( SomeCar.get_Brand().equals(SomeFilter.Brand) || SomeFilter.Brand.equals("") ) &&
                ( SomeCar.get_Model().equals(SomeFilter.Model) || SomeFilter.Model.equals("") ) &&
                ( SomeCar.get_TypeOfCar().equals(SomeFilter.Type) || SomeFilter.Type.equals("") ) &&    
                ( SomeCar.get_Country().equals(SomeFilter.Country) || SomeFilter.Country.equals("") ) &&   
                ( SomeCar.get_City().equals(SomeFilter.City) || SomeFilter.City.equals("") ) &&    
                ( SomeCar.get_Year().equals(SomeFilter.Year) || SomeFilter.Year.equals("") )                   
            )
            {
                this.CarsForShow.add(new Car(SomeCar));
            }
        }
        return true;
    }
    public boolean ShowCars()
    {
        if(this.CarsForShow == null) return false;
        javax.swing.table.DefaultTableModel model = (javax.swing.table.DefaultTableModel) Output.getModel();
        model.setRowCount(0);
        for(Car SomeCar : this.CarsForShow)
        {
            model.addRow(new Object[]
            {
                SomeCar.get_Brand(),
                SomeCar.get_Model(),
                SomeCar.get_TypeOfCar(),
                SomeCar.get_Country(),
                SomeCar.get_City(),
                SomeCar.get_Color(),
                SomeCar.get_Year(),
                SomeCar.get_Price(),
                SomeCar.get_Car_Milieage(),
                SomeCar.get_cState(),
                SomeCar.get_TypeOfFuel(),
                SomeCar.get_FuelConsumption(),
                SomeCar.get_Engine_Capacity()
            });
        }
        return true;
    }
}
